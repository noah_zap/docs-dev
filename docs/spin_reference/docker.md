Before using Spin, you should be able to create docker containers for your application and push them to the registry. Something like:

```bash
TAG="registry.nersc.gov/<myproject>/<myimage>:<mytag>"
docker build -t $TAG . && docker push $TAG
```

If you're not using external resources (for example a CFS volume), you can also create a docker-compose.yml file and run your app on your machine.

Read more about working with Docker [here](https://docs.docker.com/get-started/).

## Supported registries

Spin can run application images from [dockerhub](https://hub.docker.com/), or from the NERSC image registry at https://registry.nersc.gov/. To publish your images in the NERSC registry, you will need your project(s) created there. This process typically happens during the spin training.

