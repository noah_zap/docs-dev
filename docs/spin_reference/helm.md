One challenge associated with running complex workloads in Kubernetes is
managing and tracking the complete state of the workload, with all of its
associated containers, services, and orchestration. [Helm](https://helm.sh) is
a tool designed to simplify the process of managing such complex workflows,
specifically using a feature called
[charts](https://helm.sh/docs/topics/charts/).

[This](https://drive.google.com/file/d/1iy9bqS_GTtb2s5OzjnBRmpp0YnzbtJyZ/view?usp=sharing)
slide deck (PDF) describes NERSC user [Valerie
Hendrix](https://crd.lbl.gov/departments/data-science-and-technology/uss/staff/valerie-hendrix/)'s
process for implementing and using Helm charts to manage workflows in Spin.

### Spin-specific notes for deploying with Helm

With Spin, we recommend creating your application by hand first. Create all the volumes and secrets and make sure your app works. Next, follow these guidelines for deploying your app with Helm:

- Download the yaml files of the workloads in your app and put them in a `templates` directory.

- Also download the yaml files for every volume and tell Helm to not recreate them on every update by setting: `helm.sh/resource-policy: keep`

- creating .yaml files for secrets is also possible if you have a vault-like store where you keep them

- you can remove most of the kubernetes and rancher annotations with some *notable exceptions*:

    - `field.cattle.io/creatorId`

    - `workload.user.cattle.io/workloadselector`

- You can now pull out any configuration values (for example, env vars, ports, endpoint urls, etc.) and place them in a `values.yaml` file.

- Ingresses are a bit of a black box. It seems that most annotations can be safely removed from the ingress definition, but the rancher ui may have trouble displaying the ingress afterwards (the ingress will still work)

    - DNS propagation may take a while so you may want to keep ingresses from being recreated by setting `helm.sh/resource-policy: keep` (like for volumes)

    - to connect the ingress to a service, you need:

        - specify backend service name and port in ingress spec (`spec.rules.http.paths.backend`)

        - expose the port in the backend service spec (`spec.containers.port`)

        - You do not need any of the rancher-specific ingress annotations

- Some users reported that workloads manually edited by NERSC staff become unmanageable by helm. The only way to recover is to perform a helm uninstall and then reinstall.

- If you are missing the following in a workload that is pulling private images from your project registry, you will get a permissions error on startup when trying to pull the image:

    ```yaml
    imagePullSecrets:
    - name: registry-nersc
    ```

### Deploying to SPIN

You can deploy a helm chart via:
`helm install --set rancherUserId=$RANCHER_USERID <release-name> <chart  dir>`

or upgrade:
`helm upgrade --set rancherUserId=$RANCHER_USERID iris-test <release-name> <char dir>`

where `release-name` can be anything and `chart dir` is the directory containing your helm chart (eg. ".").

You can either set the default namespace via `kubectl`:
`kubectl config set-context --current --namespace=<your namespace>`

or, specify it via the helm commands, such as:
`helm -n $NAMESPACE -f myvalues.yaml install --set rancherUserId=$RANCHER_USERID <release-name> <char dir>`

where `-n` will set the namespace and `-f` the values file that has your environment-specific values to substitute in the templates.
