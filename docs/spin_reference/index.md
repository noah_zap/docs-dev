The Spin Reference document aims to cover all aspects of getting your application to run on Spin.

Spin lets you deploy container images of your application on computing hardware provided by NERSC. You specify information about your application (including: which version of the image to use, what types of storage you need and how to expose its endpoints to the Internet) we provide hardware and manage failover to make your scientific application a success.

Under the hood, Spin is built on [Rancher](https://rancher.com/) which uses [Kubernetes](https://kubernetes.io/) to deploy [Docker](https://www.docker.com/) or [Podman](https://podman.io/) containers to a set of compute nodes. For storage, we provide high speed networked disk access, or you can use the NERSC Global Filesystem if you need to share application state between Spin and our [Supercomputers](https://www.nersc.gov/systems/).

## Some general Spin terminology
Spin, Rancher and Kubernetes use a vast array of terms to refer to various parts of your application and the surrounding infrastructure. We will only cover some of the most frequently used terms here but if you're curious, you may look at the [full list](https://kubernetes.io/docs/reference/glossary/?fundamental=true).

* **Container image**: Blueprint for how a container is
created and started. Similar to a tarball with some other
important metadata added.

* **Container**: Running instance of an image with a
private process and storage space. Similar to a regular
process - especially a jailed one - and can have child
processes. Containers are _ephemeral_; when the
process exits, the container no longer exists.

* **Image Registry**: Versioned repository for container
images. Organized into _namespaces_ (like directories).
Labels are typically applied to images for version control.
For example,

    ```registry.nersc.gov/myproject/myimage:mylabel```

* **Pod**: One or more very-closely-coupled containers.
Pods allow for scaling; for example, a web front-end that
is heavily used might be deployed as a _pod with a scale of
three_, meaning it is configured to run three identical
containers based on the same image, and load distributed
across them.

* **Workload**: Set of parameters and rules that define
how to create and run a particular pod. Includes the image,
scale, settings such as environment variables, storage
volumes, etc.

* **Deploy**: Create a workload.

* **Ingress**: Proxy that allows a workload to be accessible
on the network using a DNS name. The ingress in Spin uses
the nginx web server software internally.

* **Namespace**: Group of workloads. Typically used to
group all of the workloads that make up a particular system,
but can also be used in other ways; for example, to group
all of the workloads that belong to a particular user.

* **Project**: Group of workloads, namespaces, ingresses, etc.
In Spin, these correspond to NERSC projects. Used for access
control; all objects in a project are accessible only to
members of the project.

* **Kubernetes**: Container scheduling system. Responsible
for running all of the above on cluster nodes.

* **Rancher**: Orchestration system for Kubernetes clusters.
Responsible for managing the Kubernetes configuration
and installation. Provides an overarching web user interface,
CLI, and internal API; provides authentication and access
control to clusters and projects.

## Accessing Spin

If you already have Spin access, you can access Spin here: https://rancher2.spin.nersc.gov/

For those wishing access to Spin, please see our resources below:

### SpinUp Workshops

NERSC conducts an instructor-led SpinUp Workshop for New Users
several times per year that include an overview of Spin concepts and
terms, a series of interactive exercises, and guidance on the design
and development process.

For current users of Spin, or users joining NERSC projects already
using Spin, there is also a Self-Guided SpinUp alternative. This
slightly abbreviated program covers concepts and terms and includes
the same interactive exercises as the instructor-led workshop.
_NERSC approval is required for this option._

### Download SpinUp Workshop Materials 

The materials for the SpinUp Workshop for New Users and Self-Guided
SpinUp are simple slide decks with embedded videos. Either version
provides a simple but thorough guide to get started.

Download the workshop materials at the
[Spin Training and Tutorials](https://www.nersc.gov/users/training/spin/)
page.



