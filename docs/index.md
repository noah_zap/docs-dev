# NERSC Technical Documentation

docs-dev is a resource with the technical details for users to make effective
use of [NERSC](https://nersc.gov)'s development and testbed resources.

!!! tip 
    These pages are hosted from a [git
    repository](https://gitlab.com/NERSC/docs-dev) and
    [contributions](https://gitlab.com/NERSC/docs-dev/blob/main/CONTRIBUTING.md)
    are welcome!

The development systems available at NERSC are shown on the left menu.

## Where to get help

If you have issues with using the Cori GPU nodes, or if you have requests for
changes or enhancements, NERSC would like to know. You can find help in two places:

  * this website
  * file a ticket in the [NERSC help desk](https://help.nersc.gov)
