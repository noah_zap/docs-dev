# Cori GPU Nodes

In 2018, 18 new nodes were added to the
[Cori](https://docs.nersc.gov/systems/cori/) system. These nodes contain
several GPU accelerators each, and are designed to facilitate GPU porting,
benchmarking, and testing efforts of NESAP codes running at NERSC, in
preparation for the [Perlmutter](https://www.nersc.gov/systems/perlmutter/)
supercomputer to be deployed at NERSC in 2020.

Information about accessing and using the Cori GPU nodes is provided through
the menu on the left side of this page.

!!! warning "Cori GPU nodes are not a production resource"
    The GPU nodes on Cori are a limited resource. They are intended to be used
    for GPU development. They are **not** intended for general purpose production
    usage. The nodes may be taken offline, rebooted, modified, etc., without
    notice. Your jobs may be killed, and you may be logged off the nodes at any
    time without notice.

## Requesting access to the Cori GPU nodes

Because the Cori GPU nodes are a development resource to help users prepare for
the Perlmutter supercomputer, users must request access to these nodes in order
to use them. The access request form can be found
[here](https://nersc.servicenowservices.com/nav_to.do?uri=%2Fcom.glideapp.servicecatalog_cat_item_view.do%3Fv%3D1%26sysparm_id%3Da715ed9edbe0ff00200f7d321f9619f5%26sysparm_link_parent%3De15706fc0a0a0aa7007fc21e1ab70c2f%26sysparm_catalog%3De0d08b13c3330100c8b837659bba8fb4%26sysparm_catalog_view%3Dcatalog_default%26sysparm_view%3Dcatalog_default).
If that link does not work, one may instead navigate to the form by going to
the [NERSC Help Portal](https://help.nersc.gov), then click "Open Request," and
then click "GPU node access."

## Where to get help

If you have issues with using the Cori GPU nodes, or if you have requests for
changes or enhancements, NERSC would like to know. You can find help in two places:

  * this website
  * file a ticket in the [NERSC Help Portal](https://help.nersc.gov)
