FROM python:3.7

WORKDIR /app
COPY ./ /app/

RUN mkdir -p /app/build_cache
RUN pip install --upgrade --cache-dir=/app/build_cache -r requirements.txt
RUN apt-get update
RUN apt-get install plantuml -y
